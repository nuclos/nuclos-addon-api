import { AddonContext } from './addon-api';
import { InjectionToken } from '@angular/core';
export const GLOBAL_CONTEXT = new InjectionToken<GlobalContext>('global-context');
/**
 * API for accessing global data from an global addon component
 */
export abstract class GlobalContext extends AddonContext {
}
