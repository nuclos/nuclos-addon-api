import { AddonContext } from './addon-api';
import { InjectionToken } from '@angular/core';

// TODO: The class DashboardContext could be injected directly - no need for an extra InjectionToken
export const DASHBOARD_CONTEXT = new InjectionToken<DashboardContext>('dashboard-context');


/**
 * API for accessing dashboard data from an dashboard addon component
 */
export declare abstract class DashboardContext extends AddonContext {
}
