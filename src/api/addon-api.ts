import { Observable } from 'rxjs';
import { IEntityObject } from './entity-object';
import { Locale } from './locale';
import { IPreference, IPreferenceContent, IPreferenceFilter, PreferenceTypeName } from './preference';
import { DialogButton, NgbModalOptions } from './dialog';
import { Component, Type } from '@angular/core';
import { MenuItem } from './menu';

export interface NavigationApi {

	/**
	 * navigates to the link defined by the given menu item name
	 * @param itemName Name of the MenuItem in Webclient
	 */
	navigateViaMenuItem(itemName: string): void;

	/**
	 * select an entry in the result list and show it in the detail form
	 * @param {IEntityObject} entityObject
	 */
	navigateToEo(entityObject: IEntityObject): void;

	/**
	 * select an entry in the result list and show it in the detail form
	 * @param {string} entityClassId
	 * @param {number | string} eoId
	 */
	navigateToEoById(entityClassId: string, eoId: number | string): void;

	/**
	 * fires when the entity class has changed on navigation
	 * @return {Observable<string>}
	 */
	onEntityClassIdChange(): Observable<string | undefined>;
}

export interface EntityObjectApi {

	/**
	 * Loads the EO for the given entity and ID.
	 */
	loadEo(entityClassId: string, entityObjectId: number): Observable<IEntityObject>;

	/**
	 * reloads the selected EO in sidebar and detail view
	 */
	reloadSelectedEo(): Observable<IEntityObject>;

	/**
	 * fires when a new EO was instantiated (not saved) in the webclient
	 * @return {Observable<EntityObject>}
	 */
	onEoAdd(): Observable<IEntityObject>;

	/**
	 * fires when another EO gets selected
	 * @return {Observable<EntityObject>}
	 */
	onEoSelection(): Observable<IEntityObject | undefined>;

	/**
	 * fires when a new EO was saved
	 * @return {Observable<EntityObject>}
	 */
	onEoCreation(): Observable<IEntityObject>;

	/**
	 * fires when the current EO has been modified
	 * @return {Observable<EntityObject>}
	 */
	onEoModification(): Observable<IEntityObject>;

	/**
	 * fires when the current EO data has been reset
	 * @return {Observable<EntityObject>}
	 */
	onEoReset(): Observable<IEntityObject>;

	/**
	 * fires when the current (existing) EO data has been saved
	 * @return {Observable<EntityObject>}
	 */
	onEoSave(): Observable<IEntityObject>;

	/**
	 * fires when the current EO data has been deleted
	 * @return {Observable<EntityObject>}
	 */
	onEoDelete(): Observable<IEntityObject>;
}

export interface PreferencesApi {

	/**
	 * creates a new unsaved Preference instance
	 * @param {string} entityClassId
	 * @param {string} app
	 * @param {string} name
	 * @param {PreferenceTypeName} type
	 * @returns {IPreference<IPreferenceContent>}
	 */
	newPreference(entityClassId: string, app: string, name: string, type?: PreferenceTypeName): IPreference<IPreferenceContent>;

	/**
	 * @return {Observable<IPreference<IPreferenceContent>>} preference
	 */
	getPreference(prefId: string): Observable<IPreference<IPreferenceContent>>;

	/**
	 * @return {Observable<Array<IPreference<PreferenceContent>>>}  array of preferences
	 */
	getPreferences(filter: IPreferenceFilter, useCache?: boolean): Observable<Array<IPreference<IPreferenceContent>>>;

	/**
	 * Saves the given preference item.
	 *
	 * If the item is new (without prefId), it is saved as new preference.
	 * If it is updated, it might be saved as 'customization' of a shared preference.
	 *
	 * @param {IPreference<any>} preferenceItem
	 * @returns {Observable<IPreference<any> | undefined>}
	 */
	savePreference(preferenceItem: IPreference<any>): Observable<IPreference<any>>;

	deletePreference(preferenceItem: IPreference<any>): Observable<boolean>;

}

export interface DialogApi {

	/**
	 * opens an alert dialog
	 * @param {string} title
	 * @param {string} message
	 * @returns {Promise<any>}
	 */
	alert(title: string, message: string): Promise<any>;

	/**
	 * opens a modal dialog
	 * interaction should be handled via ButtonOption.callback
	 * @param {string} title
	 * @param {string} message
	 * @param {DialogButton[]} buttonOptions
	 * @param {NgbModalOptions} modalOptions
	 */
	openDialog(title: string, message: string, buttonOptions: DialogButton[], modalOptions?: NgbModalOptions): void;

	/**
	 * opens an EO in a modal window
	 * @param {IEntityObject} eo
	 * @returns {Observable<any>}
	 */
	openEoInModal(eo: IEntityObject): Observable<any>;
}

/**
 * API for registering and accessing menu items via service of nuclos
 */
export declare abstract class MenuApi {

	/**
	 * register own menu item in nuclos menu bar
	 * @param item Menu-Item
	 */
	registerAdditionalMenuItem(item: MenuItem): void;

	/**
	 * returns menu structure created by nuclos
	 */
	getMenuStructure(): Observable<MenuItem>;
}

/**
 * API for accessing EntityObject data from an layout addon component
 */
export abstract class AddonContext {

	protected navigation: NavigationApi;
	protected entityobject: EntityObjectApi;
	protected preferences: PreferencesApi;
	protected dialog: DialogApi;
	protected menu: MenuApi;


	getEntityObjectApi(): EntityObjectApi {
		return this.entityobject;
	}

	getNavigationApi(): NavigationApi {
		return this.navigation;
	}

	getPreferencesApi(): PreferencesApi {
		return this.preferences;
	}

	getDialogApi(): DialogApi {
		return this.dialog;
	}

	getMenuApi(): MenuApi {
		return this.menu;
	}

	/**
	 * @return {string} FQN of the current EntityClass
	 */
	getEntityClassId(): string | undefined {
		throw ('Not implemented');
	}

	/**
	 * get the advanced property which is configured in:
	 * - layout editor for layout addon components
	 * - addon configuration for other addon components
	 * @param {string} key
	 * @return {string} property value
	 */
	getAddonProperty(key: string): string | undefined {
		throw ('Not implemented');
	}

	/**
	 * @param {string} datasourceId as FQN
	 * @param {DatasourceParams} datasourceParams as key value pairs
	 * @param {number} maxRowCount
	 * @returns {Observable<object[]>}
	 */
	executeDatasource(datasourceId: string, datasourceParams: DatasourceParams, maxRowCount?: number): Observable<object[]> {
		throw ('Not implemented');
	}

	/**
	 * @deprecated
	 *
	 *
	 * opens a modal dialog
	 * interaction should be handled via ButtonOption.callback
	 * @param {string} title
	 * @param {string} message
	 * @param {DialogButton[]} buttonOptions
	 * @param {NgbModalOptions} modalOptions
	 */
	openDialog(title: string, message: string, buttonOptions: DialogButton[], modalOptions?: NgbModalOptions): void {
		throw ('Not implemented');
	}

	/**
	 * get the factory class of a registered component
	 * used for instantiating addon components
	 * @param {string} componentName
	 * @returns {Type<Component> | undefined}
	 */
	getComponentFactoryClass(componentName: string): Type<Component> | undefined {
		throw ('Not implemented');
	}

	/**
	 * nuclos REST-API url
	 * @returns {string}
	 */
	getRestHost(): string {
		throw ('Not implemented');
	}

	/**
	 * @return The currently selected Locale.
	 */
	getLocale(): Locale {
		throw ('Not implemented');
	}
}


export declare type DatasourceParams = {
	[key: string]: any;
};
