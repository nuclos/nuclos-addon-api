export interface Locale {
	key: string;
	name: string;

	// Number format
	thousandsSeparator: string;
	decimalSeparator: string;

	// Date/time format
	datePattern: string;
	dateTimePattern: string;
	dateTimeWithSecondsPattern: string;
	javaDatePattern: string;
}
