import { Observable } from 'rxjs';
import { IEntityObject } from './entity-object';
import { AddonContext } from './addon-api';
import { InjectionToken } from '@angular/core';

// TODO: The class ResultlistContext could be injected directly - no need for an extra InjectionToken
export const RESULTLIST_CONTEXT = new InjectionToken<ResultlistContext>('resultlist-context');


/**
 * API for accessing result list data from an resultlist addon component
 */
export declare abstract class ResultlistContext extends AddonContext {

	/**
	 * fires when the result list was updated
	 * @return {Observable<boolean>}
	 */
	onEntityObjectListUpdate(): Observable<boolean>;

	/**
	 * access the result list data
	 * @return {IEntityObject[]}
	 */
	getEntityObjectList(): IEntityObject[];

	/**
	 * apply additional where condition to the result list search
	 * and execute search immediately
	 * @param {string | undefined} whereCondition
	 * @param {string} appIdentifier to distinguish multiple where conditions from different addons
	 */
	applyAdditionalResultlistWhereCondition(whereCondition: string | undefined, appIdentifier: string): void;

}

export const REQUIRED_RESULTLIST_ATTRIBUTES_KEY = 'REQUIRED_RESULTLIST_ATTRIBUTES';
export const REQUIRED_RESULTLIST_ATTRIBUTES_VIA_ADDON_PROPERTY_KEY = 'REQUIRED_RESULTLIST_ATTRIBUTES_VIA_ADDON_PROPERTY_KEY';

/**
 * decorator function to declare required attributes in the result list
 */
export function RequiredResultlistAttributes(attributes: string[]): ClassDecorator {
	const res = (ctor: any) => {
		ctor[REQUIRED_RESULTLIST_ATTRIBUTES_KEY] = attributes;
	};
	return res;
}

/**
 * decorator function to declare required attributes in the result list viy addon property names
 */
export function RequiredResultlistAttributesViaAddonProperties(addonPropertyNames: string[]): ClassDecorator {
	const res = (ctor: any) => {
		ctor[REQUIRED_RESULTLIST_ATTRIBUTES_VIA_ADDON_PROPERTY_KEY] = addonPropertyNames;
	};
	return res;
}
