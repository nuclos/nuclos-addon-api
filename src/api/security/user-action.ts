export enum UserAction {
	SharePreferences,
	CollectiveProcessing,
	ConfigureCharts,
	ConfigurePerspectives,
	PrintSearchResultList,
	WorkspaceCustomizeEntityAndSubFormColumn,
	ExecuteReports,
	ConfigureDashboards,
	BulkEdit
}
