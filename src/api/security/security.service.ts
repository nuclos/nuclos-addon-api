import { UserAction } from './user-action';

/**
 * This class can be used as injection token, the Webclient provides the implementation.
 */
export abstract class ISecurityService {
	/**
	 * Determines wether the current user is allowed to perform the given UserAction.
	 */
	abstract isUserActionAllowed(action: UserAction): boolean;
}
