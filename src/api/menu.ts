import { Subject } from 'rxjs';


export interface MenuItem {
	/**
	 * Label name showing in menu
	 */
	name: string;
	/**
	 * Angular router url to other component / service
	 */
	href?: string;
	/**
	 * URI to a custom icon
	 */
	icon?: string;
	/**
	 * Possible MenuItem positions are:
	 * 		mainmenu
	 * 		adminmenu
	 * 		usermenu
	 *
	 * e.g:
	 * let menuItem = {
	 *    ...
	 * 		position: 'mainmenu',
	 *    ...
	 * } as MenuItem;
	 *
	 * If omitted it will be put in mainmenu per default.
	 */
	position?: string;
	/**
	 * You can make a root menu entry and put sub entries below,
	 * to create a drop down menu selection.
	 */
	entries?: MenuItem[];
	/**
	 * If you want to do some code specific action in your component/service,
	 * then you can omit `href` and assign an Observable to `clickEventHandler`
	 *
	 * e.g:
	 * let clickStartenMenuEntry = new Subject<any>();
	 * let menuItem = {
	 *    ....
	 *    clickEventHandler: this.clickStartenMenuEntry,
	 *    .....
	 * } as MenuItem;
	 *
	 * this.clickStartenMenuEntry.pipe(
	 *   takeWhile(
	 *    () => this.compAlive
	 *   )
	 *  ).subscribe(() => {
	 *     // some action handling here
	 *  });
	 */
	clickEventHandler?: Subject<any>;
}
