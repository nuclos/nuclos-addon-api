import { Observable } from 'rxjs';
import { ISortModel } from './sorting';


/**
 * represents a single data entry
 */
export interface IEntityObject {

	/**
	 * entity object id
	 * @return {number} undefined if not saved
	 */
	getId(): number | undefined;

	/**
	 * get attribute value
	 * @param {string} attributeId
	 * @return {any} the attributes value
	 */
	getAttribute(attributeId: string): any;

	/**
	 * Sets the value for the attribute with the given ID
	 * and marks this EO as modified.
	 * This method does nothing if the old value equals the new value.
	 *
	 * @param attributeId
	 * @param value
	 */
	setAttribute(attributeId: string, value: any): void;

	/**
	 * Clears the attribute with the given ID.
	 * Does nothing if the attribute value is already falsy
	 * or the value is a reference pointing to nothing.
	 *
	 * @param attributeId
	 * @returns {any}
	 */
	clearAttribute(attributeId: string): any;

	/**
	 * get the list of dependent entries (subform entries)
	 * @param {string} attributeId
	 * @param {ISortModel} sortModel
	 * @return {IEntityObjectDependents}
	 */
	getDependents(referenceAttributeFqn: string, sortModel?: ISortModel): IEntityObjectDependents;

	/**
	 * get a map of dependent entries, the key indicates the referenceAttributeFqn
	 * @return {Map<string, IEntityObjectDependents>}
	 */
	getAllDependents(): Map<string, IEntityObjectDependents>;

	/**
	 * get the list of dependent entries and load them if empty
	 * @param {string} attributeId
	 * @param {ISortModel} sortModel
	 * @param {boolean} readOnly
	 * @return {IEntityObjectDependents}
	 */
	getDependentsAndLoad(attributeId: string, sortModel?: ISortModel, readOnly?: boolean): IEntityObjectDependents;

	/**
	 * Count the dependents from given attributeId
	 * @param attributeId
	 * @return {Observable<number>}
	 */
	getDependentsCount(attributeId: string): Observable<number>;

	/**
	 * @return {boolean} true if the EntityObject is not persisted
	 */
	isNew(): boolean;

	getEntityClassId(): string;

	/**
	 * Returns the temporary ID of a new, unsaved EO.
	 *
	 * @return {string}
	 */
	getTemporaryId(): string | undefined;

	/**
	 * Saves this EntityObject.
	 * @return {Observable<IEntityObject>} which returns the updated EO after save
	 */
	save(): Observable<IEntityObject>;

	/**
	 * An error that occurred e.g. during a failed generation attempt.
	 */
	getError(): string | undefined;

	isMarkedAsDeleted(): boolean;

	markAsDeleted(): void;

	/**
	 * Sets this EO as globally selected (for display/editing).
	 */
	select(): void;

	/**
	 * Determines if this EO is dirty, i.e. it has unsaved changes.
	 */
	isDirty(): boolean;

	/**
	 * Re-loads the data for this EO from the server.
	 *
	 */
	reload(): Observable<IEntityObject>;

	/**
	 * Deletes this EntityObject.
	 * The result is an Observable which returns the EO again after deletion.
	 */
	delete(): Observable<IEntityObject>;

	/**
	 * Clones current EntityObject and returns it with New-Flag
	 */
	clone(): IEntityObject;

	/**
	 * Returns EntityObjectData
	 */
	getData(): any;

	/**
	 * Update complete EntityObject data on EO
	 * @param data needs to be in right format EntityObjectData
	 */
	setData(data: any): void;

	/**
	 * Can be used to merge data changed from different sources
	 * @param data  needs to be in right format EntityObjectData
	 */
	mergeData(data: any): void;

	/**
	 * Resets this EO to the state it had last before becoming dirty.
	 * Also resets all dependents.
	 */
	reset(): void;

	getMandatorId(): string;

	getNuclosProcess(): any;

	getOwner(): any;

	clearOwner(): void;

	isLocked(): boolean;

	hasNuclosProcessChanged(): boolean;

	getRowColor(): string | undefined;

	getTextcolor(): string | undefined;

	setTextColor(textColor: string | undefined): void;

	/**
	 * Returns REST-URL for current EO if available
	 */
	getSelfURL(): string | undefined;

	/**
	 * Returns REST-URL for insert of current EO if available
	 */
	getInsertURL(): string | undefined;

	/**
	 * Returns REST-URL for clone of current EO if available
	 */
	getCloneURL(): string | undefined;

	/**
	 * Returns REST-URL for layout of current EO if available
	 */
	getLayoutURL(): string | undefined;

	/**
	 * Returns REST-URL for layout by calling service directly
	 */
	getLayoutURLDynamically(): Observable<string | undefined>;

	/**
	 * Update Layout-URL for current EO
	 */
	setLayoutURL(layoutURL: string): void;

	/**
	 * Returns ID of Layout if present
	 */
	getLayoutId(): string | undefined;

	/**
	 * Returns REST-URL for printout of current EO if available
	 */
	getPrintoutURL(): string | undefined;

	/**
	 * Define IEntityObjectEventListener to listen to specific events
	 * which may occure
	 * @param listener any type that implements IEntityObjectEventListener
	 */
	addListener(listener: IEntityObjectEventListener): void;

	/**
	 * Remove specific listener you may have added
	 * @param listener any type that implements IEntityObjectEventListener
	 */
	removeListener(listener: IEntityObjectEventListener): void;

	/**
	 * @param type any type that implements IEntityObjectEventListener
	 */
	removeListenersByType(type: any): void;

	/**
	 * Notifies client and listeners which implemented this type
	 * to display or log given message.
	 * @param errorMessage message to notify
	 */
	notifyError(errorMessage: any): void;

	/**
	 * Returns all attributes.
	 * Do not use this for modifications - use {@link setAttribute} instead.
	 *
	 * @returns {Object}
	 */
	getAttributes(): Map<string, object>;

	/**
	 * Determines if this EO is deletable.
	 * New objects are not deletable (only cancelable).
	 */
	canDelete(): boolean;

	canWrite(): boolean;

	isAttributeReadable(attributeName: string): boolean;

	isAttributeWritable(attributeName: string): boolean;

	isAttributeHidden(attributeName: string): boolean;

	/**
	 * Checks the existence of an full qualified named attribute in the entity-object.
	 * @param attributeId
	 */
	hasFqnAttribute(attributeId: string): boolean;

	/**
	 * Listen for change warnings
	 */
	observeWarnChanges(): Observable<boolean>;

	/**
	 * Trigger a warning for a change to let nuclos display popover
	 */
	triggerWarnChange(): void;

	getCreatedBy(): string | undefined;

	getCreatedAt(): Date | undefined;

	getChangedBy(): string | undefined;

	getChangedAt(): Date | undefined;
}


/**
 * subform entity object
 */
export interface ISubEntityObject extends IEntityObject {

	/**
	 * Clones this Sub-EO.
	 * See {@link EntityObject#clone()}
	 *
	 * @returns {SubEntityObject}
	 */
	clone(): ISubEntityObject;

	/**
	 * Wether this Sub-EO is selected (e.g. a selected row in a subform).
	 */
	isSelected(): boolean;

	/**
	 * parent EO
	 * @returns {IEntityObject}
	 */
	getParent(): IEntityObject;
}

/**
 * provides subform entity objects
 */
export interface IEntityObjectDependents {

	/**
	 * access subform entity objects
	 */
	asObservable(): Observable<ISubEntityObject[] | undefined>;

	/**
	 * add a list of dependents
	 */
	addAll(dependents: ISubEntityObject[]): void;

	/**
	 * remove a list of dependents
	 */
	removeAll(dependents: ISubEntityObject[]): void;

	/**
	 * set an order object, which specifies how sub-entities are ordered, when no sorting is applied
	 * @param order
	 */
	setEntityOrder(order: EntityOrder | undefined): void;

	/**
	 * set the filter string that should be applied, when calling {@link reload(sortModel?)} or
	 * {@link loadIfEmpty(sortModel?)}.
	 * The content of the preference should be of type AttributeSelectionContent otherwise this method may throw an error.
	 * @param filter the filter to collect subform entries with
	 */
	setFilter(filter: string | undefined): void;

	/**
	 * returns the filter string, that will be used when calling {@link reload(sortModel?)} or
	 * {@link loadIfEmpty(sortModel?)}.
	 */
	getFilter(): string | undefined;

	/**
	 * set the location, where new entries should be inserted
	 * @param insertLocation
	 */
	setInsertLocation(insertLocation: SubEntityInsertLocation): void;

	/**
	 * Loads all dependents for the given attribute FQN.
	 */
	loadIfEmpty(sortModel?: ISortModel, readOnly?: boolean): void;

	/**
	 * Count total of all dependents for the given attribute FQN.
	 */
	getTotalCount(): Observable<number>;

	/**
	 * The currently loaded dependents.
	 */
	current(): ISubEntityObject[] | undefined;

	clear(): void;

	set(subEos: ISubEntityObject[]): void;

	isEmpty(): boolean;

	isLoading(): boolean;

	reload(sortModel?: ISortModel): void;

	/**
	 * Reloads the dependents from the server but preserves local changes by integrating them into the incoming results
	 * @param sortModel
	 */
	reloadSoft(sortModel?: ISortModel): void;
}

export interface IEntityObjectEventListener {

	/**
	 * Triggered after an attribute value changed.
	 */
	afterAttributeChange?: (
		entityObject: IEntityObject,
		attributeName: string,
		oldValue: any,
		newValue: any
	) => any;

	/**
	 * Triggered after "Nuclos process" was changed and the default path link and layout link have been refreshed.
	 * This callback is to be used in those cases, where afterAttributeChange() is called to early, before the links related to the process
	 * change have been refreshed.
	 */
	afterProcessChange?: (
		entityObject: IEntityObject,
		oldValue: any,
		newValue: any
	) => any;

	/**
	 * Triggered when saving starts or stops.
	 */
	isSaving?: (entityObject: IEntityObject, inProgress: boolean) => any;

	afterSave?: (entityObject: IEntityObject) => any;

	afterDirty?: (entityObject: IEntityObject) => any;

	afterReload?: (entityObject: IEntityObject) => any;

	afterClone?: (entityObject: IEntityObject) => any;

	beforeCancel?: (entityObject: IEntityObject) => any;
	afterCancel?: (entityObject: IEntityObject) => any;

	afterDelete?: (entityObject: IEntityObject) => any;

	/**
	 * Triggered after the layout of the given EO was changed.
	 * E.g. after the "Nuclos process" was changed.
	 */
	afterLayoutChange?: (
		entityObject: IEntityObject,
		layoutURL: string
	) => any;

	/**
	 * Triggered after the validation status was updated.
	 */
	afterValidationChange?: (entityObject: IEntityObject) => any;

	refreshVlp?: (
		entityObject: IEntityObject,
		attributeName: string,
		onlyIfDefaultField: boolean
	) => any;

	error?: (entityObject: IEntityObject, errorMessage: any) => any;
}

export interface EntityOrder {
	/**
	 * Applies an order to the dependents array
	 * @param eod
	 * @param dependents
	 */
	(eod: IEntityObjectDependents, dependents: ISubEntityObject[]): void;
}

export enum SubEntityInsertLocation {
	INSERT_TOP,
	INSERT_BOTTOM
}
