export interface IPreferenceType {
}

export interface IPreferenceFilter {
	type?: Array<IPreferenceType>;
	app?: string;
	boMetaId?: string;
	selected?: boolean;
	layoutId?: string;
	orLayoutIsNull?: boolean;

	/**
	 * If true, only preferences for sub-BOs of the given BO with the given boMetaId should be returned.
	 */
	returnSubBoPreferences?: boolean;
}


export type PreferenceTypeName =
	'table'
	| 'subform-table'
	| 'tasklist-table'
	| 'planning-table'
	| 'searchtemplate'
	| 'subform-searchtemplate'
	| 'chart'
	| 'menu'
	| 'perspective'
	| 'dashboard'
	| 'userdefined';

export interface IPreferenceContent {
}

export interface IPreference<T> {
	type: PreferenceTypeName;
	prefId?: string;
	app?: string;
	boMetaId: string;
	layoutId?: string;
	layoutName?: string;
	boName?: string;
	name?: string;
	shared?: boolean;
	selected?: boolean;
	iconClass?: string;
	customized?: boolean;
	content: T;
	dirty?: boolean;
}

