
# Nuclos Webclient Addons

The webclient supports the following addon types:
- Layout Addon Component
- Resultlist Addon Component
- Dashboard Addon Component




## Addon types
### Layout Addon Component
The layout addon component will be enabled in the layout editor.  

The [LayoutContext](classes/LayoutContext.html) needs to be used for accessing the webclient API.

```javascript
@Inject(LAYOUT_CONTEXT) private layoutContext: LayoutContext
```

##### Addon configuration
To pass configuration data into the addon the properties must be first defined in the addon configuration.
Each defined property will then be available for configuration in the property panel of the addon inside the layout editor.

Inside the addon the properties can be accessed via:
```javascript
this.layoutContext.getAddonProperty('someProperty')
```



### Resultlist Addon Component
The Resultlist addon component will be attached to a certain position inside the webclient.

#### Addon position
The following positions are available:
- menu-bottom
- content-top
- content-bottom
- content-left
- content-right
- ( detail-toolbar)
- ...

The resultlist addon component will be instantiated from the webclient through the AddonExecutorComponent:
`
<nuc-addon-executor [addonPosition]="'content-top'"></nuc-addon-executor>
`

The [ResultlistContext](classes/ResultlistContext.html) needs to be used for accessing the webclient API.

```javascript
@Inject(RESULTLIST_CONTEXT) private resultlistContext: ResultlistContext
```

##### Addon configuration
To pass configuration data into the addon the properties must be first defined in the addon configuration.
Each defined property will then be available for configuration for each assigned BusinessObject in the property panel of the addon.

Inside the addon the properties can be accessed via:
```javascript
this.resultlistContext.getAddonProperty('someProperty')
```


##### Required resultlist attributes
If the resultlist addon component relies on certain attributes in the resultlist
then these attributes can be specified inside the addon component with the `RequiredResultlistAttributes` decorator.

Example:
```javascript
@RequiredResultlistAttributes('name', 'customerNumber')
```


Alternatively the required attributes can be defined via addon property names.
Example:
```javascript
@RequiredResultlistAttributesViaAddonProperties('nameAttributeName', 'customerNumberAttributeName')
```



### Dashboard Addon Component
The Dashboard addon component will be attached to a dashboard inside the webclient.

The [DashboardContext](classes/DashboardContext.html) needs to be used for accessing the webclient API.

```javascript
@Inject(DASHBOARD_CONTEXT) private dashboardContext: DashboardContext
```

##### Addon configuration
To pass configuration data into the addon the properties must be first defined in the addon configuration.
Each defined property will then be available for configuration in the property panel of the addon inside the dashboard editor.

Inside the addon the properties can be accessed via:
```javascript
this.dashboardContext.getAddonProperty('someProperty')
```


## Library Dependencies

Package name and version of additional dependencies needs to be specified by each addon inside its own ```package.json```.

To install addon dependencies into the webclient run:
 ```
 npm run install-addon-deps
 ```

## Addon development mode

The development mode should be used while developing addons to reduce time consuming build time.

To enable addon development mode you have to configure the system parameter ```WEBCLIENT_SRC_DIR_FOR_DEVMODE``` to point to a webclient source directory.

When enabled the addon modules will be created in ```WEBCLIENT_SRC_DIR_FOR_DEVMODE/src/addons/```.
The build process will not be started.

The webclient needs to be started with ```ng serve ...```





## Addon API deployment

### API npm repository
The addon api is deployed to a npm repository on the nexus server.
The nexus server is configured in ```package.json```, authentication is configured in ```.npmrc```.

### Deployment
Create new version and publish to nexus:

```bash
# upate the version number
npm version patch
```
```bash
# build and publish
npm run pub
```
