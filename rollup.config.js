export default {
  input: 'dist/index.js',
  output: {
    file: 'dist/bundles/npm-module-seed.umd.js',
    format: 'umd',
    name: 'nuclos-addon-api',
    moduleName: 'ng.npm-module-seed',
    globals: {
      '@angular/core': 'ng.core',
      '@angular/common': 'ng.common',
      'rxjs/Observable': 'Rx',
      'rxjs/ReplaySubject': 'Rx',
      'rxjs/add/operator/map': 'Rx.Observable.prototype',
      'rxjs/add/operator/mergeMap': 'Rx.Observable.prototype',
      'rxjs/add/observable/fromEvent': 'Rx.Observable',
      'rxjs/add/observable/of': 'Rx.Observable',
    },
  },
};
